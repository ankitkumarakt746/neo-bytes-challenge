FROM bash:latest

LABEL Maintainer="ankit"

WORKDIR /home

COPY my_first_script.sh ./

CMD [ "bash", "./test.py"]
